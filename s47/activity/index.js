// alert("?");

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", printFullName);
txtLastName.addEventListener("keyup", printFullName);

function printFullName(){
    const firstName = txtFirstName.value;
    const lastName = txtLastName.value;
    spanFullName.innerHTML = `${firstName} ${lastName}`;
};